#!/usr/bin/perl
sub VERSION_MESSAGE {
    print STDERR << "/";
vhd-merge.pl

Copyright (C) 2015 IGEL Co., Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/
}

sub HELP_MESSAGE {
    print STDERR << ".";
Usage: vhd-merge.pl [options]

  -p filename    parent vhd file
  -c filename    child vhd file
  -o filename    output vhd file
  -s             data shrink mode

.
}

use strict;
use warnings;

use Encode::Locale;
use Encode;
use Fcntl;
use IO::Seekable;
use List::Util;
use Getopt::Std;
use File::Spec;

# Get arguments
$Getopt::Std::STANDARD_HELP_VERSION = 1;
our ($opt_p, $opt_c, $opt_o, $opt_s);

if (!(getopts ("p:c:o:s") and defined $opt_p and defined $opt_c and
      defined $opt_o)) {
    HELP_MESSAGE ();
    exit (1);
}

sub vhd_error {
    die Encode::encode (locale => $_[0]->{filename}).": ".$_[1];
}

sub vhd_open {
    my $ret = {};
    my ($filename, $uuid, $timestamp) = @_;
    $ret->{filename} = $filename;

    # Open
    sysopen ($ret->{file}, Encode::encode (locale_fs => $ret->{filename}),
	     O_RDONLY | O_BINARY)
	or vhd_error ($ret, "Open file: $!\n");

    # Read vhd footer
    sysseek ($ret->{file}, -512, SEEK_END) or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, $ret->{footer}, 512) == 512
	or vhd_error ($ret, "Read footer: $!");

    # Cookie
    if (substr ($ret->{footer}, 0, 8) ne "conectix") {
	vhd_error ($ret, "Incorrect footer cookie\n");
    }

    # Compare unique ID and time stamp
    if ($uuid and $timestamp and
	(substr ($ret->{footer}, 0x44, 16) ne $uuid or
	 substr ($ret->{footer}, 0x18, 4) ne $timestamp)) {
	vhd_error ($ret, "Unique ID and time stamp mismatch\n");
    }

    # Size
    $ret->{size} = unpack ("Q>", substr ($ret->{footer}, 0x30, 8));

    # Type
    my $vhd_type = unpack ("L>", substr ($ret->{footer}, 0x3C, 4));
    if ($vhd_type == 2) {
	$ret->{fixed} = 1;
	return ($ret);
    }
    if ($vhd_type != 3 and $vhd_type != 4) {
	vhd_error ($ret, "Invalid VHD type\n");
    }

    # Read header
    sysseek ($ret->{file}, unpack ("Q>", substr ($ret->{footer}, 0x10, 8)),
	     SEEK_SET) or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, $ret->{header}, 1024) == 1024
	or vhd_error ($ret, "Read header: $!");

    # Cookie
    if (substr ($ret->{header}, 0, 8) ne "cxsparse") {
	vhd_error ($ret, "Incorrect header cookie\n");
    }

    # Read BAT
    $ret->{block_size} = unpack ("L>", substr ($ret->{header}, 0x20, 4));
    $ret->{bat_offset} = unpack ("Q>", substr ($ret->{header}, 0x10, 8));
    $ret->{bat_entries} = unpack ("L>", substr ($ret->{header}, 0x1C, 4));
    sysseek ($ret->{file}, $ret->{bat_offset}, SEEK_SET)
	or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, $ret->{bat}, $ret->{bat_entries} * 4)
	== $ret->{bat_entries} * 4 or vhd_error ($ret, "Read BAT: $!");

    if ($vhd_type == 3) {
	$ret->{dynamic} = 1;
	return ($ret);
    }
    $ret->{differencing} = 1;

    # Parent Locator Entry
    my $found = -1;
    for (my $i = 0; $i < 8; $i++) {
	my ($p_code, $p_data_space, $p_data_length, $p_reserved,
	    $p_data_offset) =
		unpack ("L>4Q>", substr ($ret->{header}, 0x240 + 24 * $i, 24));
	next if ($p_code == 0);
	if ($p_code == 0x57327275) {
	    # W2ru (Windows relative pathname)
	    if ($found != -1) {
		vhd_error ($ret, "Multiple W2ru found\n");
	    }
	    $found = $i;
	}
    }
    if ($found == -1) {
	vhd_error ($ret, "No W2ru found\n");
    }
    my ($p_code, $p_data_space, $p_data_length, $p_reserved, $p_data_offset) =
	unpack ("L>4Q>", substr ($ret->{header}, 0x240 + 24 * $found, 24));
    sysseek ($ret->{file}, $p_data_offset, SEEK_SET)
	or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, my $p_data, $p_data_length) == $p_data_length
	or vhd_error ($ret, "Read parent locator: $!");
    my $p_filename = Encode::decode ("UTF-16LE", $p_data);
    if (substr ($p_filename, 0, 2) eq ".\\" or
	substr ($p_filename, 0, 2) eq "./") {
	$p_filename = substr ($p_filename, 2);
    }
    if (index ($p_filename, "\\") != -1 or index ($p_filename, "/") != -1) {
	vhd_error ($ret, "W2ru (Windows relative pathname) contains".
		   " directory name\n");
    }
    my @path = File::Spec->splitpath ($ret->{filename});
    $path[2] = $p_filename;
    my @parent = vhd_open (File::Spec->catpath (@path),
			   substr ($ret->{header}, 0x28, 16),
			   substr ($ret->{header}, 0x38, 4));
    if ($parent[0]->{size} != $ret->{size}) {
	vhd_error ($ret, "Size mismatch\n");
    }
    return ($ret, @parent);
}

sub vhd_read {
    my ($offset, $length, $ret, @arg) = @_;
    use integer;

    if (!$ret) {
	return "\x00" x $length;
    }
    if ($offset >= $ret->{size} or $offset + $length > $ret->{size}) {
	vhd_error ($ret, "Offset ".$offset." Size ".$length." VHD Size ".
		   $ret->{size});
    }
    if ($ret->{fixed}) {
	sysseek ($ret->{file}, $offset, SEEK_SET)
	    or vhd_error ($ret, "Seek: $!");
	sysread ($ret->{file}, my $buf, $length) == $length
	    or vhd_error ($ret, "Read: $!");
	return $buf;
    }
    my $buf = "";
    while ($length > 0) {
	my $block_number = $offset / $ret->{block_size};
	my $block_offset = $offset % $ret->{block_size};
	my $block_remain = $ret->{block_size} - $block_offset;
	if ($block_remain > $length) {
	    $block_remain = $length;
	}
	if ($block_number >= $ret->{bat_entries}) {
	    vhd_error ($ret, "Block number ".$block_number.
		" Max Table Entries ".$ret->{bat_entries});
	}
	my $bitmap_lba =
	    unpack ("L>", substr ($ret->{bat}, $block_number * 4, 4));
	if ($bitmap_lba == 0xFFFFFFFF) {
	    $buf .= vhd_read ($offset, $block_remain, @arg);
	    $offset += $block_remain;
	    $length -= $block_remain;
	} else {
	    sysseek ($ret->{file}, $bitmap_lba * 0x200, SEEK_SET)
		or vhd_error ($ret, "Seek: $!");
	    sysread ($ret->{file}, my $bitmap, 0x200) == 0x200
		or vhd_error ($ret, "Read: $!");
	    my $bitmap_bits = unpack ("B4096", $bitmap);
	    while ($block_remain > 0) {
		my $tmp = substr ($bitmap_bits, $block_offset / 0x200);
		my $mode;
		if ($tmp =~ /^(0+)/) {
		    $mode = 0;
		} elsif ($tmp =~ /^(1+)/) {
		    $mode = 1;
		} else {
		    die;
		}
		my $part_len = length ($1) * 0x200 - $block_offset % 0x200;
		if ($part_len > $block_remain) {
		    $part_len = $block_remain;
		}
		if ($mode) {
		    sysseek ($ret->{file}, $bitmap_lba * 0x200 + 0x200 +
			     $block_offset, SEEK_SET)
			or vhd_error ($ret, "Seek: $!");
		    sysread ($ret->{file}, my $buf_part, $part_len) ==
			$part_len or vhd_error ($ret, "Read: $!");
		    $buf .= $buf_part;
		} else {
		    $buf .= vhd_read ($offset, $part_len, @arg);
		}
		$offset += $part_len;
		$length -= $part_len;
		$block_offset += $part_len;
		$block_remain -= $part_len;
	    }
	}
    }
    return $buf;
}

# Open
my @cmp_vhd = ();
my @vhd = vhd_open (Encode::decode (locale => $opt_c), 0, 0);
{
    my @vhd_parent = vhd_open (Encode::decode (locale => $opt_p), 0, 0);
    if ($opt_s) {
	@cmp_vhd = @vhd_parent;
	shift (@cmp_vhd);
    }
    my $parent_filename = "";
    while (1) {
	my $a = pop (@vhd);
	my $b = pop (@vhd_parent);
	if ($a->{footer} ne $b->{footer} or $a->{header} ne $b->{header}) {
	    die "Parent VHD file and child VHD file are unrelated.\n";
	}
	if ($#vhd_parent < 0) {
	    $a->{parent_filename} = $parent_filename;
	    push (@vhd, $a);
	    last;
	}
	$parent_filename = $a->{filename};
    }
}

my $bat_entries;
{
    use integer;
    $bat_entries = ($vhd[0]->{size} + 0x1FFFFF) / 0x200000;
}

# Create vhd footer
my ($vhd_sec, $vhd_head, $vhd_cyl);
{
    use integer;
    my $cylxhead;
    my $vhd_totalsec = List::Util::min ($vhd[0]->{size} / 512,
					65535 * 16 * 255);
    if ($vhd_totalsec >= 65535 * 16 * 63) {
	$vhd_sec = 255;
	$vhd_head = 16;
	$cylxhead = $vhd_totalsec / $vhd_sec;
    } else {
	$vhd_sec = 17;
	$cylxhead = $vhd_totalsec / 17;
	$vhd_head = List::Util::max (($cylxhead + 1023) / 1024, 4);
	if ($cylxhead >= $vhd_head * 1024 || $vhd_head > 16) {
	    $vhd_sec = 31;
	    $vhd_head = 16;
	    $cylxhead = $vhd_totalsec / $vhd_sec;
	}
	if ($cylxhead >= $vhd_head * 1024) {
	    $vhd_sec = 63;
	    $vhd_head = 16;
	    $cylxhead = $vhd_totalsec / $vhd_sec;
	}
    }
    $vhd_cyl = $cylxhead / $vhd_head;
}

# Get UUID and timestamp from the child file because they may be
# referred by children of the child file.
my $vhd_uuid = substr ($vhd[0]->{footer}, 0x44, 16);
my $vhd_timestamp = substr ($vhd[0]->{footer}, 0x18, 4);
my $vhd_footer =
    "conectix".
    pack ("L>2Q>", 2, 0x00010000, 512).
    pack ("a4a4L>a4", $vhd_timestamp, "IGEL", 0x00010000, "Wi2k").
    pack ("Q>2S>C2", $vhd[0]->{size}, $vhd[0]->{size}, $vhd_cyl, $vhd_head,
	  $vhd_sec).
    pack ("L>", $vhd[$#vhd]->{differencing} ? 4 : 3).
    pack ("L>", 0).
    $vhd_uuid.
    pack ("C", 0).
    pack ("C*", (0) x 427);
substr ($vhd_footer, 0x40, 4,
	pack ("L>", ~List::Util::sum (unpack ("C*", $vhd_footer))));

# Create dynamic disk header
if (length (Encode::encode ("UTF-16BE", $vhd[$#vhd]->{parent_filename}))
    > 512) {
    vhd_error ($vhd[0], "Filename too long\n");
}
my $vhd_bat_pos = 512 + 1024 + ($vhd[$#vhd]->{differencing} ? 512 : 0);
my @vhd_parent_path = File::Spec->splitpath ($vhd[$#vhd]->{parent_filename});
my $vhd_header =
    "cxsparse".
    pack ("L>8", 0xFFFFFFFF, 0xFFFFFFFF, 0, $vhd_bat_pos, 0x00010000,
	  $bat_entries, 0x200000, 0).
    substr ($vhd[$#vhd]->{header}, 0x28, 16). # UUID
    substr ($vhd[$#vhd]->{header}, 0x38, 4).  # Timestamp
    pack ("L>", 0).
    pack ("Z512", Encode::encode ("UTF-16BE", $vhd[$#vhd]->{parent_filename})).
    pack ($vhd[$#vhd]->{differencing} ? "a0a4L>3Q>" : "a24", "", "W2ru", 0x200,
	  length (Encode::encode ("UTF-16LE", $vhd_parent_path[2])), 0, 0x600).
    pack ("C*", (0) x (24 * 7 + 256));
substr ($vhd_header, 0x24, 4,
	pack ("L>", ~List::Util::sum (unpack ("C*", $vhd_header))));
my $vhd_bat_len;
{
    use integer;
    my $tmp = ($bat_entries * 4 + 511) / 512;
    $vhd_bat_len = $tmp * 512;
}
my $vhd_parent_locator = "";
if ($vhd[$#vhd]->{differencing}) {
    $vhd_parent_locator =
	pack ("Z512", Encode::encode ("UTF-16LE", $vhd_parent_path[2]));
}

# Make sure every VHD has same block size...
my $block_size = 0x200000;
for (my $i = 0; $i < $#vhd; $i++) {
    if (!$vhd[$i]->{fixed} and $vhd[$i]->{block_size} != $block_size) {
	vhd_error ($vhd[$i], "Block size is not 2MiB\n");
    }
}

# Create an empty vhd
sysopen (my $vhd_file,
	 Encode::encode (locale_fs => Encode::decode (locale => $opt_o)),
	 O_WRONLY | O_CREAT | O_EXCL | O_BINARY)
    or die "Open output file: $!\n";
syswrite ($vhd_file, $vhd_footer, 512) == 512 or die "Write: $!";
syswrite ($vhd_file, $vhd_header, 1024) == 1024 or die "Write: $!";
syswrite ($vhd_file, $vhd_parent_locator) == length ($vhd_parent_locator)
    or die "Write: $!";
$vhd_bat_pos == 512 + 1024 + length ($vhd_parent_locator) or die;
syswrite ($vhd_file, "\xff" x $vhd_bat_len, $vhd_bat_len) == $vhd_bat_len
    or die "Write: $!";
if (($vhd_bat_pos + $vhd_bat_len + 512) % 4096 > 0) {
    my $align4k = 4096 - ($vhd_bat_pos + $vhd_bat_len + 512) % 4096;
    syswrite ($vhd_file, "\x00" x $align4k, $align4k) == $align4k
	or die "Write: $!";
}
syswrite ($vhd_file, $vhd_footer, 512) == 512 or die "Write: $!";

# Copy VHD
my $zero_block = "\x00" x 0x200000;
for (my $block_number = 0; $block_number < $bat_entries; $block_number++) {
    my $merged_bitmap_bits =
	$vhd[$#vhd]->{differencing} ? "0" x 4096 : "1" x 4096;
    my $block = $zero_block;
    my $yes = 0;
    for (my $i = $#vhd; $i >= 0; $i--) {
	if ($vhd[$i]->{fixed}) {
	    my $length = $vhd[$i]->{size} - $block_number * $block_size;
	    if ($length > $block_size) {
		$length = $block_size;
	    }
	    sysseek ($vhd[$i]->{file}, $block_number * 0x200000, SEEK_SET)
		or vhd_error ($vhd[$i], "Seek: $!");
	    sysread ($vhd[$i]->{file}, $block, $length) == $length
		or vhd_error ($vhd[$i], "Read: $!");
	    $merged_bitmap_bits = "1" x 4096;
	    if ($block ne $zero_block) {
		$yes = 1;
	    }
	} elsif ((my $bitmap_lba =
		    unpack ("L>",
			    substr ($vhd[$i]->{bat}, $block_number * 4, 4))) !=
		   0xFFFFFFFF) {
	    sysseek ($vhd[$i]->{file}, $bitmap_lba * 0x200, SEEK_SET)
		or vhd_error ($vhd[$i], "Seek: $!");
	    sysread ($vhd[$i]->{file}, my $bitmap, 0x200) == 0x200
		or vhd_error ($vhd[$i], "Read: $!");
	    my $bitmap_bits = unpack ("B4096", $bitmap);
	    my $block_offset = 0;
	    my $block_remain = $block_size;
	    while ($block_remain > 0) {
		my $tmp = substr ($bitmap_bits, $block_offset / 0x200);
		my $mode;
		if ($tmp =~ /^(0+)/) {
		    $mode = 0;
		} elsif ($tmp =~ /^(1+)/) {
		    $mode = 1;
		} else {
		    die;
		}
		my $part_len = length ($1) * 0x200;
		if ($mode) {
		    sysseek ($vhd[$i]->{file},
			     $bitmap_lba * 0x200 + 0x200 + $block_offset,
			     SEEK_SET) or vhd_error ($vhd[$i], "Seek: $!");
		    sysread ($vhd[$i]->{file}, my $buf, $part_len) == $part_len
			or vhd_error ($vhd[$i], "Read: $!");
		    substr ($block, $block_offset, $part_len, $buf);
		    substr ($merged_bitmap_bits, $block_offset / 0x200,
			    $part_len / 0x200,
			    "1" x ($part_len / 0x200));
		    $yes = 1;
		}
		$block_offset += $part_len;
		$block_remain -= $part_len;
	    }
	}
    }

    # Check whether output data can be shrunk.
    if ($yes and $opt_s) {
	my $differ = 0;
	my $block_offset = 0;
	my $block_remain = $block_size;
	while ($block_remain > 0) {
	    my $tmp = substr ($merged_bitmap_bits, $block_offset / 0x200);
	    my $mode;
	    if ($tmp =~ /^(0+)/) {
		$mode = 0;
	    } elsif ($tmp =~ /^(1+)/) {
		$mode = 1;
	    } else {
		die;
	    }
	    my $part_len = length ($1) * 0x200;
	    if ($mode) {
		my $offset = $block_number * $block_size + $block_offset;
		my $len = $offset + $part_len > $vhd[0]->{size} ?
			$vhd[0]->{size} - $offset : $part_len;
		my $verify_data = vhd_read ($offset, $len, @cmp_vhd);
		if ($verify_data ne substr ($block, $block_offset, $len)) {
		    $differ = 1;
		    last;
		}
	    }
	    $block_offset += $part_len;
	    $block_remain -= $part_len;
	}
	$yes = $differ;
    }

    if ($yes) {
	my $pos = sysseek ($vhd_file, -512, SEEK_END) or die "Seek: $!";
	my $bitmap_lba = $pos / 0x200;
	my $bitmap = pack ("B4096", $merged_bitmap_bits);
	syswrite ($vhd_file, $bitmap, 0x200) == 0x200 or die "Write: $!";
	syswrite ($vhd_file, $block) == length ($block) or die "Write: $!";
	sysseek ($vhd_file, $vhd_bat_pos + $block_number * 4, SEEK_SET)
	    or die "Seek: $!";
	syswrite ($vhd_file, pack ("L>", $bitmap_lba), 4) == 4
	    or die "Write: $!";
	$pos += 512 + 0x200000 + 0xE00;
	sysseek ($vhd_file, $pos, SEEK_SET) or die "Seek: $!";
	syswrite ($vhd_file, $vhd_footer, 512) == 512 or die "Write: $!";
    }
}
