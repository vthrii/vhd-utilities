#!/usr/bin/perl
sub VERSION_MESSAGE {
    print STDERR << "/";
vhd-parent.pl

Copyright (C) 2015 IGEL Co., Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/
}

sub HELP_MESSAGE {
    print STDERR << "/";
Usage: vhd-parent.pl [options]

  -f filename    vhd file
  -r pathname    relative pathname

/
}

use strict;
use warnings;

use Encode::Locale;
use Encode;
use Fcntl;
use IO::Seekable;
use List::Util;
use Getopt::Std;

# Get arguments
$Getopt::Std::STANDARD_HELP_VERSION = 1;
our ($opt_f, $opt_r);

if (!(getopts ("f:r:") and defined $opt_f)) {
    HELP_MESSAGE ();
    exit (1);
}

# Open
sysopen (my $vhd_file, $opt_f, O_RDWR | O_BINARY) or die "Open file: $!\n";

# Read vhd footer
sysseek ($vhd_file, -512, SEEK_END) or die "Seek: $!";
sysread ($vhd_file, my $vhd_footer, 512) == 512 or die "Read footer: $!";

# Cookie
if (substr ($vhd_footer, 0, 8) ne "conectix") {
    die "Incorrect footer cookie\n";
}

# Type
my $vhd_type = unpack ("L>", substr ($vhd_footer, 0x3C, 4));
if ($vhd_type != 4) {
    die "Not differencing hard disk\n";
}

# Read header
sysseek ($vhd_file, unpack ("Q>", substr ($vhd_footer, 0x10, 8)), SEEK_SET)
    or die "Seek: $!";
sysread ($vhd_file, my $vhd_header, 1024) == 1024 or die "Read header: $!";

# Cookie
if (substr ($vhd_header, 0, 8) ne "cxsparse") {
    die "Incorrect header cookie\n";
}

# Parent Locator Entry
my $found = -1;
for (my $i = 0; $i < 8; $i++) {
    my ($p_code, $p_data_space, $p_data_length, $p_reserved, $p_data_offset) =
	unpack ("L>4Q>", substr ($vhd_header, 0x240 + 24 * $i, 24));
    next if ($p_code == 0);
    if ($p_code == 0x57327275) {
	# W2ru (Windows relative pathname)
	if ($found != -1) {
	    die "Multiple W2ru found\n";
	}
	$found = $i;
    }
}
if ($found == -1) {
    die "No W2ru found\n";
}
my ($p_code, $p_data_space, $p_data_length, $p_reserved, $p_data_offset) =
    unpack ("L>4Q>", substr ($vhd_header, 0x240 + 24 * $found, 24));
sysseek ($vhd_file, $p_data_offset, SEEK_SET) or die "Seek: $!";
if (defined $opt_r) {
    my $new_name = Encode::decode (locale => $opt_r);
    my $new_data = Encode::encode ("UTF-16LE", $new_name);
    $p_data_length = length ($new_data);
    if ($p_data_length > $p_data_space) {
	# TODO: allocate space at footer and move footer and data
	die "Name too long\n";
    }
    syswrite ($vhd_file, $new_data, $p_data_length) == $p_data_length
	or die "Write parent locator: $!";
    substr ($vhd_header, 0x240 + 24 * $found, 24,
	    pack ("L>4Q>", $p_code, $p_data_space, $p_data_length, $p_reserved,
		  $p_data_offset));
    substr ($vhd_header, 0x24, 4, pack ("L>", 0));
    substr ($vhd_header, 0x24, 4,
	    pack ("L>", ~List::Util::sum (unpack ("C*", $vhd_header))));
    sysseek ($vhd_file, unpack ("Q>", substr ($vhd_footer, 0x10, 8)), SEEK_SET)
	or die "Seek: $!";
    syswrite ($vhd_file, $vhd_header, 1024) == 1024 or die "Write header: $!";
} else {
    sysread ($vhd_file, my $p_data, $p_data_length) == $p_data_length
	or die "Read parent locator: $!";
    printf "relative %s\n", Encode::encode (locale =>
					    Encode::decode ("UTF-16LE",
							    $p_data));
}
